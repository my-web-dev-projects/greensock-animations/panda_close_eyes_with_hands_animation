import { useState } from "react";
import { PandaCEA } from "./PandaCEA";
import './index.css'

export default function App() {
  const [isEyesClose, setIsEyesClose] = useState(false);

  const peekaboo = () => {
    setIsEyesClose(!isEyesClose);
  };

  return (
    <div className="App">
      <PandaCEA isEyesClose={isEyesClose} svgWidth={300} svgHeight={250} />
      <div>
        <button type="button" onClick={peekaboo}>
          Click Me!
        </button>
      </div>
    </div>
  );
}
