# Panda_close_eyes_with_hands_animation

- This animation component is part of my upcoming project.
- You can find its working demo on [CodeSandbox](https://codesandbox.io/s/panda-close-eyes-with-hands-animation-ccjvxw?file=/src/index.js)

## Usages of this component
- The primary use of the component is to place it on the top of sign-in or register forms. When the user tries to enter a password in the password field, Panda will close its eyes with its hands.

Made with :heartpulse: by [Pranav Dalvi](https://www.linkedin.com/in/pranav-dalvi-03947a207/)